
// Require
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors=require('cors');

//Rotes Map
var routes = require('./routes/index');
var temas = require('./routes/Temas');
var tiposInsumos =require('./routes/TiposInsumo');
var insumos =require('./routes/Insumos');
var argmentos=require('./routes/Argumentos');
var subtemas=require('./routes/Subtemas');
var pavavraschave=require('./routes/PalavrasChave');
var insumodocumentos=require('./routes/InumosDocumentos');
var graucriticidade=require('./routes/GrauCriticidade');
var tipopropositura=require('./routes/TipoPropositura');
var origempropositura=require('./routes/OrigemPropositura');
var autorpropositura=require('./routes/AutorPropositura');
var situacaopropositura=require('./routes/SituacaoPropositura');
var dadosautor=require('./routes/DadosAutor');

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: false }));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//app.use(express.static('/uploads/'));

/*app.use('/resources',express.static(__dirname + '/images'));
So now, you can use http://localhost:5000/resources/myImage.jpg to serve all the images instead of http://localhost:5000/images/myImage.jpg. */
app.use('/', routes);
app.use('/v1/Temas', temas);
app.use('/v1/tipos-insumo',tiposInsumos);
app.use('/v1/insumos',insumos);
app.use('/v1/argumentos',argmentos);
app.use('/v1/subtemas',subtemas);
app.use('/v1/palavraschave',pavavraschave);
app.use('/v1/insumo-documentos',insumodocumentos);
app.use('/v1/grau-criticidade',graucriticidade);
app.use('/v1/tipo-propositura',tipopropositura);
app.use('/v1/origem-propositura',origempropositura);
app.use('/v1/autor-propositura',autorpropositura);
app.use('/v1/situacao-propositura',situacaopropositura);
app.use('/v1/dados-autor',dadosautor);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
