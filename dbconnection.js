var mssql = require("mssql"); 
var dbConfig = {
    user: 'sa',
    password: 'sa',
    server: 'localhost\\SQLEXPRESS' , 
    database: 'SIRPL'    
};

var connection = new mssql.ConnectionPool(dbConfig, function (err) {
    if (err)
        throw err; 
});

module.exports = connection; 
