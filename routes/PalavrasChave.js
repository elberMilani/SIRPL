var express = require('express');
var router = express.Router();
var db = require('../dbconnection');
var sql = require("mssql");
var bodyParser = require('body-parser');


router.get('/', function (req, res, next) {
    var request = db.request();
          request.execute('SP_BUSCAR_PALAVRAS_CHAVE', function(err, recordsets, returnValue, affected) {
            if(err) console.log(err);
                 return res.send(JSON.stringify(recordsets.recordset)); 
              
              });
  });

  /* Associar Palavras-chave  */
router.post('/insert', function (req, res, next) {
  var request = db.request();
  console.log(req.body);

  request.input('ID_INSUMO', sql.SmallInt, req.body.idinsumo);
  request.input('ID_PALAVRA_CHAVE', sql.SmallInt, req.body.idpalavrachave);
  
  request.execute('SP_INSERE_PALAVRAS_CHAVE', function (err, recordsets, returnValue, affected) {
      if (err) console.log(err);      
      console.log(returnValue);
      return res.send(recordsets);   
  });
});
module.exports = router;
