var express = require('express');
var router = express.Router();
var db = require('../dbconnection');
var sql = require("mssql");
var bodyParser = require('body-parser');

/* GET Lista de Argumentos pelo tema  . */
router.get('/getByTema/:id', function (req, res, next) {
    var request = db.request();
    request.input('ID_TEMA', sql.Int, req.params.id)
        .execute('spListarArgumentoPorTema', function (err, recordsets, returnValue, affected) {
            if (err) console.log(err);
            return res.send(JSON.stringify(recordsets.recordset));
        });
});


/* GET Lista de Argmentos  . */
router.get('/', function (req, res, next) {
    var request = db.request();
    request.input('ID_TEMA', sql.Int,null)
    .execute('spListarArgumentoPorTema', function (err, recordsets, returnValue, affected) {    
            if (err) console.log(err);
            return res.send(recordsets.recordset);
        });
});



router.delete('/delete/:id', function (req, res, next) {
    var request = db.request();
    request.input('ID_ARGUMENTO', sql.Int, null)
        .execute('spExcluirArgumentos', function (err, recordsets, returnValue, affected) {
            if (err) console.log(err);
            return res.send(JSON.stringify(recordsets.recordset));
        });
});



/* POST Inserir ARGUMENTO  */
router.post('/insert', function (req, res, next) {
    var request = db.request();
    console.log(req.body);

    request.input('DESC_ARGUMENTO', sql.SmallInt, req.body.descricao);
    request.input('IDTEMA', sql.Int, req.body.idTema);

    request.execute('spInsereArgumento', function (err, recordsets, returnValue, affected) {
        if (err) console.log(err);
        return res.send(affected);
    });
});


/* GET Lista de ARGUMENTO pelo id  . */
router.get('/getById/:id', function (req, res, next) {
    var request = db.request();
    request.input('ID_ARGUMENTO', sql.Int, req.params.id)
        .execute('spspListarArgumentoPorId', function (err, recordsets, returnValue, affected) {
            if (err) console.log(err);
            return res.send(JSON.stringify(recordsets.recordset));
        });
});

/* POST update ARGUMENTO  */
router.post('/update', function (req, res, next) {
    var request = db.request();
    console.log(req.body);


    request.input('DESCRICAO', sql.Text, req.body.descricao);
    request.input('ATIVO', sql.Bit, req.body.Ativo);
    request.input('ID_TEMA', sql.Int, req.body.idTema);
    request.input('ID_ARGUMENTO', sql.Int, req.body.id);

    request.execute('spAtualizarArgumentos', function (err, recordsets, returnValue, affected) {
        if (err) console.log(res);
        return res.send(affected);
    });
});

module.exports = router;
