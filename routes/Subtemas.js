var express = require('express');
var router = express.Router();
var db = require('../dbconnection');
var sql = require("mssql");
var bodyParser = require('body-parser');


/* GET Lista de subtemas pelo id  do tema  . */
router.get('/getSubtemas/:id', function (req, res, next) {
  var request = db.request();
  request.input('ID_TEMA', sql.Int, req.params.id)
      .execute('SP_BUSCAR_SUBTEMA_POR_TEMA', function (err, recordsets, returnValue, affected) {
          if (err) console.log(err);
          return res.send(JSON.stringify(recordsets.recordset));
      });
});

module.exports = router;
