var express = require('express');
var router = express.Router();
var db = require('../dbconnection');
var sql = require("mssql");
var bodyParser = require('body-parser');

/* GET Lista de Insumos pelo tema  . */
router.get('/getByTema/:id', function (req, res, next) {
    var request = db.request();
    request.input('IdTema', sql.Int, req.params.id)
        .execute('SP_BUSCAR_INSUMO_POR_TEMA',(err ,data)=>{
            if(err) console.error(err)
              return res.status(200).json({MSG:'Consulta Realizada com sucesso',data:data.recordset});
          });
});


/* GET Lista de Insumos pelo tema  . */ 
router.get('/', function (req, res, next) {
    var request = db.request();
    request.input('IdTema', sql.Int, req.params.id)
        .execute('SP_BUSCAR_INSUMO_POR_TEMA',(err ,data)=>{
            if(err) console.error(err)
              return res.status(200).json({MSG:'Consulta Realizada com sucesso',data:data.recordset});
          });
});



router.delete('/delete/:id', function (req, res, next) {
    var request = db.request();
    request.input('IdInsumo', sql.Int, req.params.id)
        .execute('spExcluirInsumo',(err ,data)=>{
            if(err) console.error(err)
              return res.status(200).json({MSG:'Consulta Realizada com sucesso',data:data.recordset});
          });
});



/* POST Inserir insumo  */
router.post('/insert', function (req, res, next) {
    var request = db.request();
    console.log(req.body);

    request.input('ID_TIPO_INSUMO', sql.SmallInt, req.body.idtipoinsumo);
    request.input('ID_TEMA', sql.SmallInt, req.body.idtema);
    request.input('ID_SUB_TEMA', sql.SmallInt, req.body.idsubtema);
    request.input('TITULO', sql.VarChar, req.body.titulo);
    request.input('DESCCRICAO', sql.VarChar, req.body.descricao);
    request.input('PALAVRAS_CHAVE', sql.SmallInt, req.body.palavraschave);
    request.input('ENDERECO', sql.VarChar, req.body.endereco);
    request.input('AIVO', sql.Bit, req.body.ativo);
    request.input('DT_INSUMO', sql.Int, req.body.dtinsumo);
    request.output('ID_INSUMO_OUT', sql.Int,req.body.idinsumo);
    request.input('VERSIONAR', sql.Bit,req.body.versionar);


/*     request.execute('SP_INSERE_INSUMO', function (err, recordsets, returnValue, affected) {
        if (err) console.log(err);      
        console.log(returnValue);
        return res.send(recordsets.output);   
    }); */
    request.execute('SP_INSERE_INSUMO',(err ,data)=>{
        if(err) console.error(err)
          return res.status(200).json({MSG:'Consulta Realizada com sucesso',data:data.output});
      });
});


/* GET Lista de Insumos pelo id  . */
router.get('/getById/:id', function (req, res, next) {
    var request = db.request();
    request.input('IdInsumo', sql.Int, req.params.id)
        .execute('SP_BUSCAR_INSUMO_PELO_ID', function (err, recordsets, returnValue, affected) {
            if (err) console.log(err);
            return res.send(recordsets.recordset);
        });
});

/* POST update insumo  */
router.post('/update', function (req, res, next) {
    var request = db.request();
    console.log(req.body);

    request.input('IdTipoInsumo', sql.SmallInt, req.body.IdTipoInsumo);
    request.input('NomeInsumo', sql.VarChar, req.body.NomeInsumo);
    request.input('LinkInsumo', sql.VarChar, req.body.Link);
    request.input('TagInsumo', sql.VarChar, req.body.Tags);
    request.input('DescricaoInsumo', sql.VarChar, req.body.Descricao);
    request.input('Ativo', sql.Bit, req.body.Ativo);
    request.input('IdTema', sql.Int, req.body.IdTema);
    request.input('IdInsumo', sql.Int, req.body.IdInsumo);

    request.execute('SP_ATUALIZAR_INSUMO', function (err, recordsets, returnValue, affected) {
        if (err) console.log(res);
        return res.send(affected);
    });
});

module.exports = router;
