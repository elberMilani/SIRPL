var express = require('express');
var router = express.Router();
var db = require('../dbconnection');
var sql = require("mssql");
var bodyParser = require('body-parser');


/* GET Lista de subtemas pelo id  do tema  . */
router.get('/getDadosAutor/:id', function (req, res, next) {
  var request = db.request();
  request.input('COD_PESSOA', sql.Int, req.params.id)
      .execute('SP_BUSCAR_DADOS_AUTOR', function (err, recordsets, returnValue, affected) {
          if (err) console.log(err);
          return res.send(JSON.stringify(recordsets.recordset));
      });
});

module.exports = router;
